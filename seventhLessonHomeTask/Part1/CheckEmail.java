
package seventhLessonHomeTask.Part1;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CheckEmail {
    private static String email;
    public CheckEmail(String email){
        this.email = email;
    }

    public static boolean onChekEmailMethod(){

//        System.out.println(email);
        // проверка содержит ли email символ @, если нет, то email введен не правильно
        if (email.lastIndexOf('@') != -1) {

            // проверка не является ли имя email пустым
            char[] bufferNameEmail = new char[email.lastIndexOf('@')];
            email.getChars(0, email.lastIndexOf('@'), bufferNameEmail, 0);
//            System.out.println(bufferNameEmail);
            if (bufferNameEmail.length != 0) {

                // проверка на наличие домена второго уровня
                char[] bufferSecondLevelDomen = new char[email.lastIndexOf('.') - email.lastIndexOf('@') - 1];
                email.getChars(email.lastIndexOf('@') + 1, email.lastIndexOf('.'), bufferSecondLevelDomen, 0);
//                System.out.println(bufferSecondLevelDomen);
                if (bufferNameEmail.length != 0){

                    // проверка на наличие домена первого уровня
                    char[] bufferFirstLevelDomen = new char[email.length() - email.lastIndexOf('.')];
                    email.getChars(email.lastIndexOf('.'), email.length(), bufferFirstLevelDomen, 0);
//                    System.out.println(bufferFirstLevelDomen);
                    if (bufferFirstLevelDomen.length != 0){

                        // проверка email на наличие недопустимых символов
                        char[] bufferGeneralDomen = new char[email.length() - email.lastIndexOf('@') - 1];
                        email.getChars(email.lastIndexOf('@') + 1, email.length(), bufferGeneralDomen, 0);
                        String buffer = String.valueOf(bufferGeneralDomen);
                        Pattern pattern = Pattern.compile("(\\w*).+(\\W)[.]?(\\w*)");
                        Matcher matcher = pattern.matcher(buffer);
//                        System.out.println("matcher " + matcher.matches());
//                        System.out.println(buffer);
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
}
