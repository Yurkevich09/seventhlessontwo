
package seventhLessonHomeTask.Part2.src.com.shelpuksoftcorp.SignUP;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main2 extends Application {
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage window) throws Exception {
        window.setScene(new Scene(FXMLLoader.load(getClass().getResource("/views/SingUpWindow.fxml")),
                300, 400));
        window.setTitle("Create new account");
        window.centerOnScreen();
        window.show();
    }
}
