
package seventhLessonHomeTask.Part2.src.com.shelpuksoftcorp.controller;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import seventhLessonHomeTask.Part1.CheckEmail;
import seventhLessonHomeTask.Part2.src.com.shelpuksoftcorp.SignUP.Main2;

import java.net.URL;
import java.util.ResourceBundle;

class SingUpController implements Initializable {
    @FXML
    public TextArea emailTextArea;
    @FXML
    public TextArea firstPasswordTextArea;
    @FXML
    public TextArea secondPasswordTextArea;
    @FXML
    public Button signUpButton;
    @FXML
    public Text passwordInformation;
    @FXML
    public Text emailInformation;
    @FXML
    public Text processInformation;
    @FXML
    public CheckBox agreeCeckBox;

    public String email;
    public String firstPassword;
    public String secondPassword;
    public int click = 0;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        signUpButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> onSingUpMouseClicked());
    }

    public void onSingUpMouseClicked() {


        emailInformation.setFill(Color.GREEN);
        emailInformation.setText("Email is correct!");

        email = emailTextArea.getText();
        firstPassword = firstPasswordTextArea.getText();
        secondPassword = secondPasswordTextArea.getText();

        CheckEmail checkEmail = new CheckEmail(email);
        boolean email1 = checkEmail.onChekEmailMethod();
//        System.out.println(email);
        if (email1){
            if (firstPassword.equals(secondPassword)
                    & (firstPassword.length() != 0 | secondPassword.length() != 0)){
                emailTextArea.setDisable(true);
                firstPasswordTextArea.setDisable(true);
                secondPasswordTextArea.setDisable(true);
                agreeCeckBox.setDisable(true);

                passwordInformation.setFill(Color.GREEN);
                passwordInformation.setText("Password is correct!");
                processInformation.setFill(Color.GREEN);
                processInformation.setText("Congratulations! You just finished registration successfully.");

                System.out.println("Email: " + email + "\nPassword: " + firstPassword);
                click++;
            }
            else {
                passwordInformation.setFill(Color.RED);
                firstPasswordTextArea.clear();
                secondPasswordTextArea.clear();
                passwordInformation.setText("Passwords don't match!");
            }
        }
        else {
            emailTextArea.clear();
            emailInformation.setFill(Color.RED);
            emailInformation.setText("Email isn't correct.");
        }

        if(emailTextArea.isDisabled()){
            signUpButton.setText("Exit");
            if (click > 2){
                Platform.exit();
            }

        }
    }

    public void onCheckBoxMouseClicked() {
        if (agreeCeckBox.isSelected()){
            signUpButton.setDisable(false);
        }
        else {
            signUpButton.setDisable(true);
        }
    }
}
