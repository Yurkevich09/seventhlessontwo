package seventhLessonHomeTask.Part1;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList <String> email = new ArrayList<>();
        email.add("kirill.shelpuk@gmail.com");
        email.add("harrison@ford@tut.by");
        email.add("steven.spilberg@stor?mnet.net");
        email.add("justin.bieber.mail.ru");
        email.add("michael@phelps.yandex,by");
        email.add("@gmail.com");
        for (int i = 0; i < email.size(); i++){
            CheckEmail checkEmail = new CheckEmail(email.get(i));
            System.out.println(checkEmail.onChekEmailMethod() + "\n");
        }
    }
}
